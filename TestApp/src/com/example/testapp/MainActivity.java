package com.example.testapp;

import java.util.ArrayList;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {
	

	
	private ArrayList<String> m_strArrJSONValues = new ArrayList<String>();
	private ListView m_vListViewMainActiviy;
	private ArrayAdapter<String> m_Adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		m_vListViewMainActiviy = (ListView) findViewById(R.id.list_view_main_activity);
		PendingIntent pi = createPendingResult(0, new Intent(), 0);
		Log.d("myLogs", "PIcreate");
		
		Intent i = new Intent(this, JSONService.class).putExtra("pi", pi);
		startService(i);
		

	}
	@Override
	protected void onResume() {
		
		super.onResume();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		m_strArrJSONValues =(ArrayList<String>) data.getSerializableExtra("JSON");
		m_Adapter = new ArrayAdapter<String>(getApplicationContext(),
				android.R.layout.simple_list_item_1);
		
		m_vListViewMainActiviy.setAdapter(m_Adapter);
		m_Adapter.addAll(m_strArrJSONValues);
		m_Adapter.notifyDataSetChanged();
		super.onActivityResult(requestCode, resultCode, data);
	}
	

	


	
}
