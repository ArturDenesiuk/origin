package com.example.testapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

public class JSONService extends Service {
	public final static String URL = "http://echo.jsontest.com/key1/value1/key2/value2/key3/value3/key4/value4";
	
	ArrayList<String> m_strArrJSONValues = new ArrayList<String>();
	AsyncTask<URL, Void, Void> task;
	PendingIntent m_intent;

	@Override
	public void onCreate() {
		Log.d("myLogs", "Service create");
	 task = new AsyncTask<URL, Void, Void>() {
			
			
			
			@Override
			protected Void doInBackground(URL... params) {
				
				try {

					URLConnection conn = params[0].openConnection();
					BufferedReader in = new BufferedReader(
							new InputStreamReader(conn.getInputStream()));
					String line;
					StringBuffer buffer = new StringBuffer();
					while ((line = in.readLine()) != null) {
						buffer.append(line);
					}

					JSONObject obj = new JSONObject(buffer.toString());
					for (int i = 0; i < 4; i++) {
						m_strArrJSONValues.add(obj.getString("key" + (i + 1))) ;

					}	
					
					Collections.sort(m_strArrJSONValues);
					
					

				} catch (MalformedURLException e) {

					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				Intent i = new Intent().putExtra("JSON", m_strArrJSONValues);
				Log.d("myLogs", "onPostExec");
				try {
					m_intent.send(getApplicationContext(), 0, i);
				} catch (CanceledException e) {
					
					e.printStackTrace();
				}
				JSONService.this.stopSelf();
				super.onPostExecute(result);
			}

		};
		
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		m_intent = intent.getParcelableExtra("pi");
		Log.d("myLogs", "Service start");
		try {
			task.execute(new URL(URL));
		} catch (MalformedURLException e) {
			
			e.printStackTrace();
		}
		
		
		return START_STICKY;
	}
	

	@Override
	public IBinder onBind(Intent intent) {
	
		return null;
	}

	

	

}
